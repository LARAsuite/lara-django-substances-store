"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_substances_store admin *

:details: lara_django_substances_store admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev forms_generator -c lara_django_substances_store > forms.py" to update this file
________________________________________________________________________
"""
# django crispy forms s. https://github.com/django-crispy-forms/django-crispy-forms for []
# generated with django-extensions forms_generator -c  [] > forms.py (LARA-version)

from django import forms
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Row, Column

from .models import ExtraData, SubstanceInstance, SubstanceOrderItem, SubstancesWishlist, SubstanceBasket, SubstancesOrder, SubstancesLocalStore, PolymerInstance, PolymerOrderItem, PolymerWishlist, PolymerBasket, PolymersOrder, PolymerLocalStore, MixtureComponentInstance, MixtureInstance, MixtureOrderItem, MixtureWishlist, MixtureBasket, MixturesOrder, MixtureLocalStore 

class ExtraDataCreateForm(forms.ModelForm):
    class Meta:
        model = ExtraData
        fields = (
                'data_type',
                'namespace',
                'URI',
                'text',
                'XML',
                'JSON',
                'media_type',
                'IRI',
                'URL',
                'description',
                'file',
                'image')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'data_type',
                'namespace',
                'URI',
                'text',
                'XML',
                'JSON',
                'media_type',
                'IRI',
                'URL',
                'description',
                'file',
                'image',
            Submit('submit', 'Create')
        )

class ExtraDataUpdateForm(forms.ModelForm):
    class Meta:
        model = ExtraData
        fields = (
                'data_type',
                'namespace',
                'URI',
                'text',
                'XML',
                'JSON',
                'media_type',
                'IRI',
                'URL',
                'description',
                'file',
                'image')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'data_type',
                'namespace',
                'URI',
                'text',
                'XML',
                'JSON',
                'media_type',
                'IRI',
                'URL',
                'description',
                'file',
                'image',
            Submit('submit', 'Create')
        )

class SubstanceInstanceCreateForm(forms.ModelForm):
    class Meta:
        model = SubstanceInstance
        fields = (
                'namespace',
                'substance',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'description',
                'amount_vol',
                'amount_mass',
                'purity',
                'labware',
                'hazard_classes',
                'hazard_statements',
                'precautionary_statements',
                'hazard_symbols',
                'storage_class',
                'literature',
                )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                 'substance',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'description',
                'amount_vol',
                'amount_mass',
                'purity',
                'labware',
                'hazard_classes',
                'hazard_statements',
                'precautionary_statements',
                'hazard_symbols',
                'storage_class',
                'literature',
               
            Submit('submit', 'Create')
        )

class SubstanceInstanceUpdateForm(forms.ModelForm):
    class Meta:
        model = SubstanceInstance
        fields = (
                'namespace',
                'substance',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'description',
                'amount_vol',
                'amount_mass',
                'purity',
                'labware',
                'hazard_classes',
                'hazard_statements',
                'precautionary_statements',
                'hazard_symbols',
                'storage_class',
                'literature',
                )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'substance',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'description',
                'amount_vol',
                'amount_mass',
                'purity',
                'labware',
                'hazard_classes',
                'hazard_statements',
                'precautionary_statements',
                'hazard_symbols',
                'storage_class',
                'literature',
                
            Submit('submit', 'Update')
        )

class SubstanceOrderItemCreateForm(forms.ModelForm):
    class Meta:
        model = SubstanceOrderItem
        fields = (
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'substance_inst')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'substance_inst',
            Submit('submit', 'Create')
        )

class SubstanceOrderItemUpdateForm(forms.ModelForm):
    class Meta:
        model = SubstanceOrderItem
        fields = (
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'substance_inst')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'substance_inst',
            Submit('submit', 'Update')
        )

class SubstancesWishlistCreateForm(forms.ModelForm):
    class Meta:
        model = SubstancesWishlist
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
            Submit('submit', 'Create')
        )

class SubstancesWishlistUpdateForm(forms.ModelForm):
    class Meta:
        model = SubstancesWishlist
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
            Submit('submit', 'Update')
        )

class SubstanceBasketCreateForm(forms.ModelForm):
    class Meta:
        model = SubstanceBasket
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
            Submit('submit', 'Create')
        )

class SubstanceBasketUpdateForm(forms.ModelForm):
    class Meta:
        model = SubstanceBasket
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
            Submit('submit', 'Update')
        )

class SubstancesOrderCreateForm(forms.ModelForm):
    class Meta:
        model = SubstancesOrder
        fields = (
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state',
            Submit('submit', 'Create')
        )

class SubstancesOrderUpdateForm(forms.ModelForm):
    class Meta:
        model = SubstancesOrder
        fields = (
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state',
            Submit('submit', 'Update')
        )

class SubstancesLocalStoreCreateForm(forms.ModelForm):
    class Meta:
        model = SubstancesLocalStore
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location',
            Submit('submit', 'Create')
        )

class SubstancesLocalStoreUpdateForm(forms.ModelForm):
    class Meta:
        model = SubstancesLocalStore
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location',
            Submit('submit', 'Update')
        )

class PolymerInstanceCreateForm(forms.ModelForm):
    class Meta:
        model = PolymerInstance
        fields = (
                'namespace',
                'polymer',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'description',
                'amount_vol',
                'amount_mass',
                'purity',
                'labware',
                'hazard_classes',
                'hazard_statements',
                'precautionary_statements',
                'hazard_symbols',
                'storage_class',
                'literature',
                )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'polymer',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'description',
                'amount_vol',
                'amount_mass',
                'purity',
                'labware',
                'hazard_classes',
                'hazard_statements',
                'precautionary_statements',
                'hazard_symbols',
                'storage_class',
                'literature',
                
            Submit('submit', 'Create')
        )

class PolymerInstanceUpdateForm(forms.ModelForm):
    class Meta:
        model = PolymerInstance
        fields = (
                'namespace',
                'polymer',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'description',
                'amount_vol',
                'amount_mass',
                'purity',
                'labware',
                'hazard_classes',
                'hazard_statements',
                'precautionary_statements',
                'hazard_symbols',
                'storage_class',
                'literature',
                )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'polymer',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'description',
                'amount_vol',
                'amount_mass',
                'purity',
                'labware',
                'hazard_classes',
                'hazard_statements',
                'precautionary_statements',
                'hazard_symbols',
                'storage_class',
                'literature',
                
            Submit('submit', 'Update')
        )

class PolymerOrderItemCreateForm(forms.ModelForm):
    class Meta:
        model = PolymerOrderItem
        fields = (
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'polymer_inst')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'polymer_inst',
            Submit('submit', 'Create')
        )

class PolymerOrderItemUpdateForm(forms.ModelForm):
    class Meta:
        model = PolymerOrderItem
        fields = (
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'polymer_inst')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'polymer_inst',
            Submit('submit', 'Update')
        )

class PolymerWishlistCreateForm(forms.ModelForm):
    class Meta:
        model = PolymerWishlist
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
            Submit('submit', 'Create')
        )

class PolymerWishlistUpdateForm(forms.ModelForm):
    class Meta:
        model = PolymerWishlist
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
            Submit('submit', 'Create')
        )

class PolymerBasketCreateForm(forms.ModelForm):
    class Meta:
        model = PolymerBasket
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
            Submit('submit', 'Create')
        )

class PolymerBasketUpdateForm(forms.ModelForm):
    class Meta:
        model = PolymerBasket
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
            Submit('submit', 'Create')
        )

class PolymersOrderCreateForm(forms.ModelForm):
    class Meta:
        model = PolymersOrder
        fields = (
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state',
            Submit('submit', 'Create')
        )

class PolymersOrderUpdateForm(forms.ModelForm):
    class Meta:
        model = PolymersOrder
        fields = (
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state',
            Submit('submit', 'Create')
        )

class PolymerLocalStoreCreateForm(forms.ModelForm):
    class Meta:
        model = PolymerLocalStore
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location',
            Submit('submit', 'Create')
        )

class PolymerLocalStoreUpdateForm(forms.ModelForm):
    class Meta:
        model = PolymerLocalStore
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location',
            Submit('submit', 'Create')
        )

class MixtureComponentInstanceCreateForm(forms.ModelForm):
    class Meta:
        model = MixtureComponentInstance
        fields = (
                'namespace',
                'substance',
                'polymer',
                'concentration',
                'concentration_weight',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'description',
                'amount_vol',
                'amount_mass',
                'purity',
                'labware',
                'hazard_classes',
                'hazard_statements',
                'precautionary_statements',
                'hazard_symbols',
                'storage_class',
                'literature',
                )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'substance',
                'polymer',
                'concentration',
                'concentration_weight',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'description',
                'amount_vol',
                'amount_mass',
                'purity',
                'labware',
                'hazard_classes',
                'hazard_statements',
                'precautionary_statements',
                'hazard_symbols',
                'storage_class',
                'literature',
                
            Submit('submit', 'Create')
        )

class MixtureComponentInstanceUpdateForm(forms.ModelForm):
    class Meta:
        model = MixtureComponentInstance
        fields = (
                'namespace',
                'substance',
                'polymer',
                'concentration',
                'concentration_weight',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'description',
                'amount_vol',
                'amount_mass',
                'purity',
                'labware',
                'hazard_classes',
                'hazard_statements',
                'precautionary_statements',
                'hazard_symbols',
                'storage_class',
                'literature',
               )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'substance',
                'polymer',
                'concentration',
                'concentration_weight',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'description',
                'amount_vol',
                'amount_mass',
                'purity',
                'labware',
                'hazard_classes',
                'hazard_statements',
                'precautionary_statements',
                'hazard_symbols',
                'storage_class',
                'literature',
               
            Submit('submit', 'Update')
        )

class MixtureInstanceCreateForm(forms.ModelForm):
    class Meta:
        model = MixtureInstance
        fields = (
                'namespace',
                'components',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'description',
                'amount_vol',
                'amount_mass',
                'purity',
                'labware',
                'hazard_classes',
                'hazard_statements',
                'precautionary_statements',
                'hazard_symbols',
                'storage_class',
                'literature',
                )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'components',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'description',
                'amount_vol',
                'amount_mass',
                'purity',
                'labware',
                'hazard_classes',
                'hazard_statements',
                'precautionary_statements',
                'hazard_symbols',
                'storage_class',
                'literature',
                
            Submit('submit', 'Create')
        )

class MixtureInstanceUpdateForm(forms.ModelForm):
    class Meta:
        model = MixtureInstance
        fields = (
                'namespace',
                'components',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'description',
                'amount_vol',
                'amount_mass',
                'purity',
                'labware',
                'hazard_classes',
                'hazard_statements',
                'precautionary_statements',
                'hazard_symbols',
                'storage_class',
                'literature',
                )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'components',
                'name',
                'name_full',
                'barcode1D',
                'barcode2D',
                'UUID',
                'URL',
                'handle',
                'IRI',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'manufacturing_date',
                'price',
                'currency',
                'purchase_date',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'hash_SHA256',
                'description',
                'amount_vol',
                'amount_mass',
                'purity',
                'labware',
                'hazard_classes',
                'hazard_statements',
                'precautionary_statements',
                'hazard_symbols',
                'storage_class',
                'literature',
                
            Submit('submit', 'Update')
        )

class MixtureOrderItemCreateForm(forms.ModelForm):
    class Meta:
        model = MixtureOrderItem
        fields = (
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'mixture_inst')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'mixture_inst',
            Submit('submit', 'Create')
        )

class MixtureOrderItemUpdateForm(forms.ModelForm):
    class Meta:
        model = MixtureOrderItem
        fields = (
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'mixture_inst')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'mixture_inst',
            Submit('submit', 'Update')
        )

class MixtureWishlistCreateForm(forms.ModelForm):
    class Meta:
        model = MixtureWishlist
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
            Submit('submit', 'Create')
        )

class MixtureWishlistUpdateForm(forms.ModelForm):
    class Meta:
        model = MixtureWishlist
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
            Submit('submit', 'Update')
        )

class MixtureBasketCreateForm(forms.ModelForm):
    class Meta:
        model = MixtureBasket
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
            Submit('submit', 'Create')
        )

class MixtureBasketUpdateForm(forms.ModelForm):
    class Meta:
        model = MixtureBasket
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
            Submit('submit', 'Create')
        )

class MixturesOrderCreateForm(forms.ModelForm):
    class Meta:
        model = MixturesOrder
        fields = (
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state',
            Submit('submit', 'Create')
        )

class MixturesOrderUpdateForm(forms.ModelForm):
    class Meta:
        model = MixturesOrder
        fields = (
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state',
            Submit('submit', 'Update')
        )

class MixtureLocalStoreCreateForm(forms.ModelForm):
    class Meta:
        model = MixtureLocalStore
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location',
            Submit('submit', 'Create')
        )

class MixtureLocalStoreUpdateForm(forms.ModelForm):
    class Meta:
        model = MixtureLocalStore
        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location',
            Submit('submit', 'Update')
        )

# from .forms import ExtraDataCreateForm, SubstanceInstanceCreateForm, SubstanceOrderItemCreateForm, SubstancesWishlistCreateForm, SubstanceBasketCreateForm, SubstancesOrderCreateForm, SubstancesLocalStoreCreateForm, PolymerInstanceCreateForm, PolymerOrderItemCreateForm, PolymerWishlistCreateForm, PolymerBasketCreateForm, PolymersOrderCreateForm, PolymerLocalStoreCreateForm, MixtureComponentInstanceCreateForm, MixtureInstanceCreateForm, MixtureOrderItemCreateForm, MixtureWishlistCreateForm, MixtureBasketCreateForm, MixturesOrderCreateForm, MixtureLocalStoreCreateFormExtraDataUpdateForm, SubstanceInstanceUpdateForm, SubstanceOrderItemUpdateForm, SubstancesWishlistUpdateForm, SubstanceBasketUpdateForm, SubstancesOrderUpdateForm, SubstancesLocalStoreUpdateForm, PolymerInstanceUpdateForm, PolymerOrderItemUpdateForm, PolymerWishlistUpdateForm, PolymerBasketUpdateForm, PolymersOrderUpdateForm, PolymerLocalStoreUpdateForm, MixtureComponentInstanceUpdateForm, MixtureInstanceUpdateForm, MixtureOrderItemUpdateForm, MixtureWishlistUpdateForm, MixtureBasketUpdateForm, MixturesOrderUpdateForm, MixtureLocalStoreUpdateForm
