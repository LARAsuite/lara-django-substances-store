"""
________________________________________________________________________

:PROJECT: LARA

*Export substances to lara substance store database*

:details: export substances from lara substance store database
         -

:file:    export_substances.py
:authors: mark doerr (mark.doerr@uni-greifswald.de)

:date: (creation)          20180324
:date: (last modification) 2020-01-09

.. note:: -
.. todo:: -
________________________________________________________________________
"""
__version__ = "0.0.1"


import os
import sys
import shutil
import re
import logging
import subprocess
import json
import csv

from django.db import IntegrityError
from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ObjectDoesNotExist

from django.conf import settings
#~ from lara_projects.models import Project, Experiment

from lara_django_base.models import Namespace, Location
from lara_django_people.models import Entity
from lara_django_substances.models import Substance, Polymer, Mixture
from lara_django_substances_store.models import SubstanceInstance, PolymerInstance, MixtureInstance
#~ from .chem_info import ChemInfo
from lara_django_sequences.models import SequenceClass, Sequence

class ExportSubstancesStore(BaseCommand):
    """Adding substances to django database, could be later expanded to other file formats,
       in this case, the file opening should be moved to the processing unit """
    def __init__(self, event=None, replace_data=False):
        super().__init__()

    def exportSubstancesSafetyCSV(self, csv_filename):
        """adding substances using information from CSV file 
           direct pdf output can be achieved using python-pdfkit
           https://github.com/JazzCore/python-pdfkit

           import pdfkit
           pdfkit.from_string('Hello!', 'out.pdf')
           options = {...} # s. python-pdfkit documentation
           pdfkit.from_string(output_string, 'out.pdf', options=options)
        """
        try:
            with open(csv_filename, 'w') as csvfile:
                fieldnames = [ "Name", "GHS Classification",
                               "Hazardous Classes" , "Location in Lab",
                               "Amount", "Safety Sheet", "Substitution possible ?" ]

                writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

                writer.writeheader()
                for substance in SubstanceInstance.objects.all():
                    subst_row_dic = {}
                    try:
                        subst_row_dic['Name'] = substance.substance.name
                        subst_row_dic['GHS Classification'] = substance.hazard_symbols
                        subst_row_dic['Hazardous Classes'] = substance.hazard_statements
                        subst_row_dic['Location in Lab'] = substance.location.name

                        if substance.amount_vol is not None :
                            subst_row_dic['Amount'] = f"{substance.amount_vol:.3f} L"
                        elif substance.amount_mass is not None :
                            subst_row_dic['Amount'] = f"{substance.amount_mass:.3f} kg"
                        subst_row_dic['Safety Sheet'] = "available"
                        subst_row_dic['Substitution possible ?'] = "no"

                        writer.writerow(subst_row_dic)
                    except Exception as err:
                        self.stderr.write('file {}, export Error: {}'.format(csv_filename, err))

        except Exception as err:
            self.stderr.write('file {}, GeneralError: {}'.format(csv_filename, err))
            exit()

        self.stdout.write(self.style.SUCCESS("All substances exported ! Have a nice day ;)"))

    def exportSubstancesSafetyMD(self, md_filename):
        """adding substances using information from Markdow file """
        
        try:
            with open(md_filename, 'w') as mdfile:
                fieldnames = [ "Name", "GHS Classification",
                               "Hazardous Classes" , "Location in Lab",
                               "Amount", "Safety Sheet", "Substitution possible ?" ]

                mdfile.write(f"| {' | '.join(fieldnames)} |\n" )
                table_separtor = '| -------------------------- |:-------------------:|:--------------:|:-------------:|:-----------:|:-----------:|:----------:|:---:|\n'
                #mdfile.write( f'{len(fieldnames) * "--- | "} ---\n' )
                mdfile.write(table_separtor)

                for substance in SubstanceInstance.objects.all():
                    try:
                        
                        if substance.amount_vol is not None :
                            subst_amount = f"{substance.amount_vol:.3f} L"
                        elif substance.amount_mass is not None :
                            subst_amount = f"{substance.amount_mass:.3f} kg"

                        subst_row = ( f"{substance.substance.name} | {substance.hazard_symbols} | " 
                                      f"{substance.hazard_statements} | {substance.location.name} | "
                                      f"{subst_amount} | available | no \n" )

                        mdfile.write(subst_row )
                    except Exception as err:
                        self.stderr.write('file {}, export Error: {}'.format(md_filename, err))

        except Exception as err:
            self.stderr.write('file {}, GeneralError: {}'.format(md_filename, err))
            exit()
  
        success_str = (f"You can used pandoc to make a nice pdf (mind that wkhtmlt needs to be installed) \n\n" 
                       f"pandoc -t html5 --from markdown safety_data.md --css=table_borders.css  -o safety_data.pdf\n\n"
                       f"All substances exported ! Have a nice day ;)")
        
        self.stdout.write(self.style.SUCCESS(success_str))

