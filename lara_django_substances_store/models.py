"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_substances_store models *

:details: lara_django_substances_store database models.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - remove unwanted models/fields
________________________________________________________________________
"""


import logging
from datetime import date, datetime, timedelta
import uuid
from random import randint

from django.utils import timezone
from django.conf import settings
from django.utils.translation import gettext_lazy as _

from django.db import models

from lara_django_base.models import DataType, MediaType, ExtraDataAbstr, Tag, PhysicalStateMatter
from lara_django_store.models import ItemInstanceAbstr, OrderItemAbstr, ItemOrderAbstr, ItemBasketAbstr

from lara_django_substances.models import Substance, Polymer, MixtureComponent, Mixture
from lara_django_material_store.models import LabwareInstance
from lara_django_people.models import Entity
#from lara_django_processes.models import ProcdureInstance # !! circular import
from lara_django_library.models import LibItem
from lara_django_base.models import ExtraDataAbstr, Currency, DataType, MediaType, Namespace, Tag, Location, ItemStatus

settings.FIXTURES += []


class ExtraData(ExtraDataAbstr):
    """This class can be used to extend data, by extra information,
       e.g. more telephone numbers, customer numbers, ... """
    extra_data_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)  # models.AutoField(primary_key=True)
    file = models.FileField(
        upload_to='substances_store', blank=True, null=True, help_text="rel. path/filename")
    image = models.ImageField(upload_to='substances_store/images/', blank=True, default="image.svg",
                              help_text="extra substance image")


class SubstanceInstanceAbstr(ItemInstanceAbstr):
    #date_opened = models.DateField(default="0001-01-01", help_text="")
    #date_best_before = models.DateField(default="0001-01-01", help_text="")

    amount_vol = models.DecimalField(
        max_digits=20, decimal_places=9, blank=True, null=True, help_text="in L")
    amount_mass = models.DecimalField(
        max_digits=20, decimal_places=9, blank=True, null=True, help_text="in g")
    purity = models.DecimalField(
        blank=True, null=True, decimal_places=2, max_digits=9, help_text="in /1")
    pH = models.DecimalField(null=True, blank=True, decimal_places=4, max_digits=6, help_text="pH value in /1")
    physical_state_matter = models.ForeignKey(PhysicalStateMatter, related_name="%(app_label)s_%(class)s_state_of_matter_related",
                                        related_query_name="%(app_label)s_%(class)s_state_of_matter_related_query",
                                        on_delete=models.CASCADE, null=True, blank=True,
                                        help_text="physical state of matter at 293 K")
    labware = models.ForeignKey(LabwareInstance, related_name="%(app_label)s_%(class)s_containers_related",
                                related_query_name="%(app_label)s_%(class)s_containers_related_query",
                                on_delete=models.CASCADE, null=True, blank=True,
                                help_text="location of container")

    # to be implemented: need to be subdevided into separate classes
    # could also be moved to reference table
    hazard_classes = models.TextField(
        blank=True, null=True, help_text="Safety hazard classes.")
    # could also be moved to reference table
    hazard_statements = models.TextField(
        blank=True, null=True, help_text="Safety hazared statements.")
    precautionary_statements = models.TextField(
        blank=True, null=True, help_text="Saftey precautionary statements.")  # could also be moved to reference table
    # could also be moved to reference table
    hazard_symbols = models.TextField(blank=True, null=True, help_text="")
    storage_class = models.TextField(blank=True, null=True, help_text="")
    users = models.ManyToManyField(Entity, related_name="%(app_label)s_%(class)s_extra_data_related",
                                   related_query_name="%(app_label)s_%(class)s_extra_data_related_query", blank=True,
                                   help_text="people, who are using and taking care of the substance")
    literature = models.URLField(
        blank=True, null=True, help_text="literature regarding this instance of a substance")
    # literature = models.ManyToManyField(LibItem, related_name="%(app_label)s_%(class)s_literature_related",
    #                                     related_query_name="%(app_label)s_%(class)s_literature_related_query",  blank=True,
    #                                     help_text="literature regarding this instance of a substance")

    extra_data = models.ManyToManyField(ExtraData, related_name="%(app_label)s_%(class)s_extra_data_related",
                                        related_query_name="%(app_label)s_%(class)s_extra_data_related_query", blank=True,
                                        help_text="e.g. manual")

    class Meta:
        abstract = True


class SubstanceInstance(SubstanceInstanceAbstr):
    """Substance Instance"""
    substance_instance_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    substance = models.ForeignKey(Substance, related_name="%(app_label)s_%(class)s_subtances_related",
                                  related_query_name="%(app_label)s_%(class)s_substances_related_query",
                                  on_delete=models.CASCADE, null=True, blank=True)
    

    def save(self,  *args, force_insert=None, using=None, **kwargs):
        """ Here we generate some default values for full_name """
        if self.name is None or self.name == "" :
            self.name = self.substance.name

        
        if self.name_full is None or self.name_full == "":
            if self.namespace is not None:
               self.name_full = f"{self.namespace.name}/" + "_".join((self.name.replace(' ', '_') , self.registration_no, self.barcode1D))
            else:
                self.name_full = "_".join((self.name, self.registration_no, self.barcode1D))
            
        super().save(*args, force_insert=force_insert, using=using,  **kwargs)


    def __str__(self):
        return self.substance.name or ""

    def __repr__(self):
        return self.substance.name or ""


class SubstanceOrderItem(OrderItemAbstr):
    """ Substance Order Item Model for ordering substances ...
    :param OrderItemAbstr: _description_
    :type OrderItemAbstr: _type_
    """
    substance_orderitem_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    substance_inst = models.ForeignKey(SubstanceInstance, related_name='%(app_label)s_%(class)s_substances_instance_related',
                                       related_query_name="%(app_label)s_%(class)s_substances_instance_related_query",
                                       on_delete=models.CASCADE)
    extra_data = models.ManyToManyField(ExtraData, blank=True, related_name='%(app_label)s_%(class)s_oi_extra_data_related',
                                        related_query_name="%(app_label)s_%(class)s_oi_extra_data_related_query",
                                        help_text="extra data")

    def __str__(self):
        return self.substance_inst.substance.name or ""

    def __repr__(self):
        return self.substance_inst.substance.name or ""


class SubstancesWishlist(ItemBasketAbstr):
    """ Substances Wishlist Model for wishing substances ...
    :param ItemWishlistAbstr: _description_
    :type ItemWishlistAbstr: _type_
    """
    substance_wishlist_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    substance_wish = models.ManyToManyField(SubstanceOrderItem, related_name='%(app_label)s_%(class)s_substance_wish_related',
                                            related_query_name="%(app_label)s_%(class)s_substance_wish_related_query",
                                            blank=True, help_text="substances wished (type: OrderSubstances)")

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""


class SubstanceBasket(ItemBasketAbstr):
    """Substances Basket Model for ordering substances and put them into a basket """
    substance_basket_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    substances = models.ManyToManyField(SubstanceOrderItem, related_name='%(app_label)s_%(class)s_substances_basket_related',
                                        related_query_name="%(app_label)s_%(class)s_substances_basket_related_query",
                                        blank=True, help_text="substances in basket")

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""


class SubstancesOrder(ItemOrderAbstr):
    """ Order Substances Model ...
    :param ItemOrderAbstr: _description_
    :type ItemOrderAbstr: _type_
    """
    substances_order_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    substance_order_items = models.ManyToManyField(SubstanceOrderItem, related_name='%(app_label)s_%(class)s_substances_order_related',
                                                   related_query_name="%(app_label)s_%(class)s_substances_order_related_query",
                                                   blank=True, help_text="substances ordered")
    extra_data = models.ManyToManyField(ExtraData, blank=True, related_name='%(app_label)s_%(class)s_po_extra_data_related',
                                        related_query_name="%(app_label)s_%(class)s_po_extra_data_related_query",
                                        help_text="extra data")

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""

    # class Meta:
        # ordering = ['-date_created']
        # verbose_name = "Order Substancess"
        #verbose_name_plural = "Order Substancess"


class SubstancesLocalStore(ItemBasketAbstr):
    """ Substances Local Store Model ...
        this can be used to store substances in a local store, e.g. in a lab"""
    substance_localstore_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    substance_instances = models.ManyToManyField(SubstanceInstance, related_name='%(app_label)s_%(class)s_substances_inst_loc_related',
                                                 related_query_name="%(app_label)s_%(class)s_substances_inst_loc_related_query",
                                                 blank=True, help_text="substances in local store")
    location = models.ForeignKey(Location, related_name='%(app_label)s_%(class)s_substances_loc_related',
                                 related_query_name="%(app_label)s_%(class)s_substances_loc_related_query",
                                 on_delete=models.CASCADE)

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""


class PolymerInstance(SubstanceInstanceAbstr):
    """Polymer Instance"""
    polymer_instance_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    polymer = models.ForeignKey(Polymer, related_name="%(app_label)s_%(class)s_polymers_related",
                                related_query_name="%(app_label)s_%(class)s_polymers_related_query",
                                on_delete=models.CASCADE, null=True, blank=True)
    
    def save(self,  *args, force_insert=None, using=None, **kwargs):
        """ Here we generate some default values for full_name """
        if self.name is None or self.name == "":
            self.name = self.polymer.name

        
        if self.name_full is None or self.name_full == "":
            if self.namespace is not None:
               self.name_full = f"{self.namespace.name}/" + "_".join((self.name.replace(' ', '_'), self.registration_no, self.barcode1D))
            else:
                self.name_full = "_".join((self.name, self.registration_no, self.barcode1D))
            
        super().save(*args, force_insert=force_insert, using=using,  **kwargs)

    def __str__(self):
        return self.polymer.name or ""

    def __repr__(self):
        return self.polymer.name or ""


# _______________ Ordering / Wishlists ______________


class PolymerOrderItem(OrderItemAbstr):
    """ Polymer Order Item Model for ordering polymers ...
    :param OrderItemAbstr: _description_
    :type OrderItemAbstr: _type_
    """
    polymer_orderitem_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    polymer_inst = models.ForeignKey(PolymerInstance, related_name='%(app_label)s_%(class)s_polymers_instance_related',
                                     related_query_name="%(app_label)s_%(class)s_polymers_instance_related_query",
                                     on_delete=models.CASCADE)
    extra_data = models.ManyToManyField(ExtraData, blank=True, related_name='%(app_label)s_%(class)s_oi_extra_data_related',
                                        related_query_name="%(app_label)s_%(class)s_oi_extra_data_related_query",
                                        help_text="extra data")

    def __str__(self):
        return self.polymer.name_full or ""

    def __repr__(self):
        return self.polymer.name_full or ""


class PolymerWishlist(ItemBasketAbstr):
    """ Polymer Wishlist Model for wishing polymers ...
    :param ItemWishlistAbstr: _description_
    :type ItemWishlistAbstr: _type_
    """
    polymer_wishlist_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    polymer_wish = models.ManyToManyField(PolymerOrderItem, related_name='%(app_label)s_%(class)s_polymer_wish_related',
                                          related_query_name="%(app_label)s_%(class)s_polymer_wish_related_query",
                                          blank=True, help_text="polymers wished (type: OrderPolymers)")

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""


class PolymerBasket(ItemBasketAbstr):
    """Polymer Basket Model for ordering polymers and put them into a basket """
    polymer_basket_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    polymers = models.ManyToManyField(PolymerOrderItem, related_name='%(app_label)s_%(class)s_polymers_basket_related',
                                      related_query_name="%(app_label)s_%(class)s_polymers_basket_related_query",
                                      blank=True, help_text="polymers in basket")

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""


class PolymersOrder(ItemOrderAbstr):
    """ Order Polymers Model ...
    :param ItemOrderAbstr: _description_
    :type ItemOrderAbstr: _type_
    """
    polymers_order_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    polymer_order_items = models.ManyToManyField(PolymerOrderItem, related_name='%(app_label)s_%(class)s_polymers_order_related',
                                                 related_query_name="%(app_label)s_%(class)s_polymers_order_related_query",
                                                 blank=True, help_text="polymers ordered")
    extra_data = models.ManyToManyField(ExtraData, blank=True, related_name='%(app_label)s_%(class)s_po_extra_data_related',
                                        related_query_name="%(app_label)s_%(class)s_po_extra_data_related_query",
                                        help_text="extra data")

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""

    # class Meta:
        # ordering = ['-date_created']
        # verbose_name = "Order Polymers"
        #verbose_name_plural = "Order Polymers"


class PolymerLocalStore(ItemBasketAbstr):
    """ Polymer Local Store Model ...
        this can be used to store polymers in a local store, e.g. in a lab"""
    polymer_localstore_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    polymer_instances = models.ManyToManyField(PolymerInstance, related_name='%(app_label)s_%(class)s_polymers_inst_loc_related',
                                               related_query_name="%(app_label)s_%(class)s_polymers_inst_loc_related_query",
                                               blank=True, help_text="polymers in local store")
    location = models.ForeignKey(Location, related_name='%(app_label)s_%(class)s_polymers_loc_related',
                                 related_query_name="%(app_label)s_%(class)s_polymers_loc_related_query",
                                 on_delete=models.CASCADE)

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""

    
class MixtureComponentInstance(SubstanceInstanceAbstr):
    """Mixture Component Instance"""
    mix_comp_store_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)  
    
    IRI = models.URLField(
        blank=True,  null=True, unique=True, max_length=512, help_text="International Resource Identifier - IRI: is used for semantic representation ")
    substance = models.ForeignKey(SubstanceInstance, related_name='%(app_label)s_%(class)s_substances_related',
                                  related_query_name="%(app_label)s_%(class)s_substances_related_query", blank=True, null=True,
                                  on_delete=models.CASCADE, help_text="substance instance")
    polymer = models.ForeignKey(PolymerInstance, related_name='%(app_label)s_%(class)s_polymers_related', blank=True, null=True,
                                related_query_name="%(app_label)s_%(class)s_polymers_related_query",
                                on_delete=models.CASCADE, help_text="polymer instance")
    concentration = models.DecimalField(max_digits=20, decimal_places=9, blank=True, null=True,
                                        help_text="concentration in mol/l")  # this is used for mixtures
    concentration_weight = models.DecimalField(max_digits=20, decimal_places=9, blank=True, null=True,
                                               help_text="concentration in kg/l")  # this is used for mixtures
    description = models.TextField(
        blank=True, null=True, help_text="description")
    
    def save(self,  *args, force_insert=None, using=None, **kwargs):
        """ Here we generate some default values for full_name """
        if self.name is None or self.name == "" :
            if self.substance is not None:
                self.name = self.substance.name
            elif self.polymer is not None:
                self.name = self.polymer.name

        
        if self.name_full is None or self.name_full == "":
            if self.namespace is not None:
               self.name_full = f"{self.namespace.name}/" + "_".join((self.name.replace(' ', '_'), self.registration_no, self.barcode1D))
            else:
                self.name_full = "_".join((self.name, self.registration_no, self.barcode1D))
            
        super().save(*args, force_insert=force_insert, using=using,  **kwargs)

    def __str__(self):
        return self.name or ""

    def __repr__(self):
        return self.name or ""



class MixtureInstance(SubstanceInstanceAbstr):
    """Mixture Instance"""
    mixture_store_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    components = models.ManyToManyField(MixtureComponentInstance, related_name='%(app_label)s_%(class)s_components_related',
                                        related_query_name="%(app_label)s_%(class)s_components_related_query",
                                        help_text="components of the mixture instance")
    mixture = models.ForeignKey(Mixture, related_name='%(app_label)s_%(class)s_mixtures_related', blank=True, null=True,
                                related_query_name="%(app_label)s_%(class)s_mixtures_related_query",
                                on_delete=models.CASCADE, help_text="abstract reference mixture")
    # this porduces a circular import
    # procedure = models.ForeignKey("ProcdureInstance", related_name='%(app_label)s_%(class)s_procedures_related',
    #                                 related_query_name="%(app_label)s_%(class)s_procedures_related_query", blank=True, null=True,
    #                                 on_delete=models.CASCADE, help_text="procedure instance of how the mixture was made")
    extra_data = models.ManyToManyField(ExtraData, blank=True, related_name='%(app_label)s_%(class)s_mi_extra_data_related',
                                        related_query_name="%(app_label)s_%(class)s_mi_extra_data_related_query",
                                        help_text="extra data")
    
    def save(self,  *args, force_insert=None, using=None, **kwargs):
        """ Here we generate some default values for full_name """
        # if self.name == "" or self.name is None:
        #     self.name = self.mixture.name

        
        if self.name_full is None or self.name_full == "":
            if self.namespace is not None:
               self.name_full = f"{self.namespace.name}/" + "_".join((self.name.replace(' ', '_'), self.registration_no, self.barcode1D))
            else:
                self.name_full = "_".join((self.name, self.registration_no, self.barcode1D))
            
        super().save(*args, force_insert=force_insert, using=using,  **kwargs)

    def __str__(self):
        return self.name or ""

    def __repr__(self):
        return self.name or ""

# _______________ Ordering / Wishlists ______________


class MixtureOrderItem(OrderItemAbstr):
    """ Mixture Order Item Model for ordering mixtures ...
    :param OrderItemAbstr: _description_
    :type OrderItemAbstr: _type_
    """
    mixture_orderitem_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    mixture_inst = models.ForeignKey(MixtureInstance, related_name='%(app_label)s_%(class)s_mixtures_instance_related',
                                     related_query_name="%(app_label)s_%(class)s_mixtures_instance_related_query",
                                     on_delete=models.CASCADE)
    extra_data = models.ManyToManyField(ExtraData, blank=True, related_name='%(app_label)s_%(class)s_oi_extra_data_related',
                                        related_query_name="%(app_label)s_%(class)s_oi_extra_data_related_query",
                                        help_text="extra data")

    def __str__(self):
        return self.mixture.name_full or ""

    def __repr__(self):
        return self.mixture.name_full or ""


class MixtureWishlist(ItemBasketAbstr):
    """ Mixture Wishlist Model for wishing mixtures ...
    :param ItemWishlistAbstr: _description_
    :type ItemWishlistAbstr: _type_
    """
    mixture_wishlist_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    mixture_wish = models.ManyToManyField(MixtureOrderItem, related_name='%(app_label)s_%(class)s_mixture_wish_related',
                                          related_query_name="%(app_label)s_%(class)s_mixture_wish_related_query",
                                          blank=True, help_text="mixtures wished (type: OrderMixtures)")

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""


class MixtureBasket(ItemBasketAbstr):
    """Mixture Basket Model for ordering mixtures and put them into a basket """
    mixture_basket_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    mixtures = models.ManyToManyField(MixtureOrderItem, related_name='%(app_label)s_%(class)s_mixtures_basket_related',
                                      related_query_name="%(app_label)s_%(class)s_mixtures_basket_related_query",
                                      blank=True, help_text="mixtures in basket")

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""


class MixturesOrder(ItemOrderAbstr):
    """ Order Mixtures Model ...
    :param ItemOrderAbstr: _description_
    :type ItemOrderAbstr: _type_
    """
    mixtures_order_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    mixture_order_items = models.ManyToManyField(MixtureOrderItem, related_name='%(app_label)s_%(class)s_mixtures_order_related',
                                                 related_query_name="%(app_label)s_%(class)s_mixtures_order_related_query",
                                                 blank=True, help_text="mixtures ordered")
    extra_data = models.ManyToManyField(ExtraData, blank=True, related_name='%(app_label)s_%(class)s_po_extra_data_related',
                                        related_query_name="%(app_label)s_%(class)s_po_extra_data_related_query",
                                        help_text="extra data")

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""

    # class Meta:
        # ordering = ['-date_created']
        # verbose_name = "Order Mixtures"
        #verbose_name_plural = "Order Mixtures"


class MixtureLocalStore(ItemBasketAbstr):
    """ Mixture Local Store Model ...
        this can be used to store mixtures in a local store, e.g. in a lab"""
    mixture_localstore_id = models.UUIDField(
        primary_key=True, default=uuid.uuid4, editable=False)
    mixture_instances = models.ManyToManyField(MixtureInstance, related_name='%(app_label)s_%(class)s_mixtures_inst_loc_related',
                                               related_query_name="%(app_label)s_%(class)s_mixtures_inst_loc_related_query",
                                               blank=True, help_text="mixtures in local store")
    location = models.ForeignKey(Location, related_name='%(app_label)s_%(class)s_mixtures_loc_related',
                                 related_query_name="%(app_label)s_%(class)s_mixtures_loc_related_query",
                                 on_delete=models.CASCADE)

    def __str__(self):
        return self.name_full or ""

    def __repr__(self):
        return self.name_full or ""
