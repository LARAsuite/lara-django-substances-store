"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_substances_store views *

:details: lara_django_substances_store views module.
         - add app specific urls here
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: -
________________________________________________________________________
"""

from dataclasses import dataclass, field
from typing import List

from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.urls import reverse

from django.views.generic import DetailView, ListView, CreateView, UpdateView, DeleteView

from django_tables2 import SingleTableView

from .forms import SubstanceInstanceCreateForm, SubstanceInstanceUpdateForm, PolymerInstanceCreateForm, PolymerInstanceUpdateForm, MixtureComponentInstanceCreateForm, MixtureComponentInstanceUpdateForm, MixtureInstanceCreateForm, MixtureInstanceUpdateForm
from .tables import SubstanceInstanceTable, PolymerInstanceTable, MixtureComponentInstanceTable, MixtureInstanceTable

# Create your  lara_django_substances_store views here.

from .models import SubstanceInstance, PolymerInstance, MixtureInstance



@dataclass
class SubstancesStoreMenu:
    menu_items:  List[dict] = field(default_factory=lambda: [   
        {'name': 'Substances',
         'path': 'lara_django_substances:substance-list'},
        {'name': 'Substance-Store',
                 'path': 'lara_django_substances_store:substance-list'},
        {'name': 'Polymers',
         'path': 'lara_django_substances:polymer-list'},
         {'name': 'Polymer-Store',
         'path': 'lara_django_substances_store:polymer-list'},
        {'name': 'Mixtures',
          'path': 'lara_django_substances:mixture-list'},
        {'name': 'Mixture-Store',
          'path': 'lara_django_substances_store:mixture-list'}  
    ])

class SubstanceInstanceSingleTableView(SingleTableView):
    model = SubstanceInstance
    table_class = SubstanceInstanceTable

    #fields = ('name', 'name_full', 'URL', 'handle', 'IRI', 'manufacturer', 'model_no', 'product_type', 'type_barcode', 'product_no', 'weight', 'specifications', 'spec_JSON', 'spec_doc', 'brochure', 'quickstart', 'manual', 'service_manual', 'icon', 'image', 'description', 'substance_id', 'substance_class', 'shape')

    template_name = 'lara_django_substances_store/list.html'
    success_url = '/substances/substance/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Substance Store (Instances) - List"
        context['create_link'] = 'lara_django_substances_store:substance-create'
        context['menu_items'] = SubstancesStoreMenu().menu_items
        return context


class SubstanceInstanceDetailView(DetailView):
    model = SubstanceInstance

    template_name = 'lara_django_substances_store/substance_instance_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Substance Store (Instance) - Details"
        context['update_link'] = 'lara_django_substances_store:substance-update'
        context['menu_items'] = SubstancesStoreMenu().menu_items
        return context


class SubstanceInstanceCreateView(CreateView):
    model = SubstanceInstance

    template_name = 'lara_django_substances_store/create_form.html'
    form_class = SubstanceInstanceCreateForm
    success_url = '/substances/substance/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Substance Store (Instance) - Create"
        return context


class SubstanceInstanceUpdateView(UpdateView):
    model = SubstanceInstance

    template_name = 'lara_django_substances_store/update_form.html'
    form_class = SubstanceInstanceUpdateForm
    success_url = '/substances/substance/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Substance Store (Instance) - Update"
        context['delete_link'] = 'lara_django_substances_store:substance-delete'
        return context


class SubstanceInstanceDeleteView(DeleteView):
    model = SubstanceInstance

    template_name = 'lara_django_substances_store/delete_form.html'
    success_url = '/substances/substance/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Substance Store (Instance) - Delete"
        context['delete_link'] = 'lara_django_substances_store:substance-delete'
        return context

class PolymerInstanceSingleTableView(SingleTableView):
    model = PolymerInstance
    table_class = PolymerInstanceTable

    #fields = ('name', 'name_full', 'URL', 'handle', 'IRI', 'manufacturer', 'model_no', 'product_type', 'type_barcode', 'product_no', 'weight', 'specifications', 'spec_JSON', 'spec_doc', 'brochure', 'quickstart', 'manual', 'service_manual', 'icon', 'image', 'description', 'polymer_id', 'polymer_class', 'shape')

    template_name = 'lara_django_substances_store/list.html'
    success_url = '/substances-store/polymer/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Polymer Store (Instance) - List"
        context['create_link'] = 'lara_django_substances_store:polymer-create'
        context['menu_items'] = SubstancesStoreMenu().menu_items
        return context


class PolymerInstanceDetailView(DetailView):
    model = PolymerInstance

    template_name = 'lara_django_substances_store/polymer_instance_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Polymer Store (Instance) - Details"
        context['update_link'] = 'lara_django_substances_store:polymer-update'
        context['menu_items'] = SubstancesStoreMenu().menu_items
        return context


class PolymerInstanceCreateView(CreateView):
    model = PolymerInstance

    template_name = 'lara_django_substances_store/create_form.html'
    form_class = PolymerInstanceCreateForm
    success_url = '/substances-store/polymer/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Polymer Store (Instance) - Create"
        return context


class PolymerInstanceUpdateView(UpdateView):
    model = PolymerInstance

    template_name = 'lara_django_substances_store/update_form.html'
    form_class = PolymerInstanceUpdateForm
    success_url = '/substances-store/polymer/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Polymer Store (Instance) - Update"
        context['delete_link'] = 'lara_django_substances_store:polymer-delete'
        return context


class PolymerInstanceDeleteView(DeleteView):
    model = PolymerInstance

    template_name = 'lara_django_substances_store/delete_form.html'
    success_url = '/substances-store/polymer/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Polymer Store (Instance) - Delete"
        context['delete_link'] = 'lara_django_substances_store:polymer-delete'
        return context

class MixtureInstanceSingleTableView(SingleTableView):
    model = MixtureInstance
    table_class = MixtureInstanceTable

    #fields = ('name', 'name_full', 'URL', 'handle', 'IRI', 'manufacturer', 'model_no', 'product_type', 'type_barcode', 'product_no', 'weight', 'specifications', 'spec_JSON', 'spec_doc', 'brochure', 'quickstart', 'manual', 'service_manual', 'icon', 'image', 'description', 'mixture_id', 'mixture_class', 'shape')

    template_name = 'lara_django_substances_store/list.html'
    success_url = '/substances-store/mixture/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Mixture Store (Instance) - List"
        context['create_link'] = 'lara_django_substances_store:mixture-create'
        context['menu_items'] = SubstancesStoreMenu().menu_items
        return context


class MixtureInstanceDetailView(DetailView):
    model = MixtureInstance

    template_name = 'lara_django_substances_store/mixture_instance_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Mixture Store (Instance) - Details"
        context['update_link'] = 'lara_django_substances_store:mixture-update'
        context['menu_items'] = SubstancesStoreMenu().menu_items
        return context


class MixtureInstanceCreateView(CreateView):
    model = MixtureInstance

    template_name = 'lara_django_substances_store/create_form.html'
    form_class = MixtureInstanceCreateForm
    success_url = '/substances-store/mixture/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Mixture Store (Instance) - Create"
        return context


class MixtureInstanceUpdateView(UpdateView):
    model = MixtureInstance

    template_name = 'lara_django_substances_store/update_form.html'
    form_class = MixtureInstanceUpdateForm
    success_url = '/substances-store/mixture/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Mixture Store (Instance) - Update"
        context['delete_link'] = 'lara_django_substances_store:mixture-delete'
        return context


class MixtureInstanceDeleteView(DeleteView):
    model = MixtureInstance

    template_name = 'lara_django_substances_store/delete_form.html'
    success_url = '/substances-store/mixture/list'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['section_title'] = "Mixture Store (Instance) - Delete"
        context['delete_link'] = 'lara_django_substances_store:mixture-delete'
        return context


## ----- old ------

class SubstancesListView_old(ListView):
    model = SubstanceInstance
    template_name = 'lara_django_substances_store/substance_list.html'

    table_caption = "List of all LARA substances"
    context = {'table_caption': table_caption}



class IndexView(ListView):
    model = SubstanceInstance
    ordering = ['registration_no']

    #paginate_by = 50

    template_name = 'lara_django_substances_store/index.html'

    context_object_name = 'substance_list'


class SubstanceSafetyView(ListView):
    model = SubstanceInstance
    ordering = ['registration_no']

    template_name = 'lara_django_substances_store/safety.html'
    context_object_name = 'substance_list'


class SubstanceDetail_old(DetailView):
    model = SubstanceInstance
