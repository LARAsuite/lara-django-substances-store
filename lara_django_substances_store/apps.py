"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_substances_store app *

:details: lara_django_substances_store app configuration. 
         This provides a generic django app configuration mechanism.
         For more details see:
         https://docs.djangoproject.com/en/4.0/ref/applications/
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - 
________________________________________________________________________
"""


from django.apps import AppConfig


class LaraDjangoSubstancesStoreConfig(AppConfig):
    name = 'lara_django_substances_store'
    # enter a verbose name for your app: lara_django_substances_store here - this will be used in the admin interface
    verbose_name = 'LARA-django Substances Store'
    # lara_app_icon = 'lara_django_substances_store_icon.svg'  # this will be used to display an icon, e.g. in the main LARA menu.
