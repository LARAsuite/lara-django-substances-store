"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_substances_store urls *

:details: lara_django_substances_store urls module.
         - add app specific urls here
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: -
________________________________________________________________________
"""

from . import views
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include
from django.views.generic import TemplateView
#import lara_django.urls as base_urls

from . import views

# Add your {cookiecutter.project_slug}} urls here.


# !! this sets the apps namespace to be used in the template
app_name = "lara_django_substances_store"

# companies and institutions should also be added
# the 'name' attribute is used in templates to address the url independent of the view

urlpatterns = [
    path('substance/list/', views.SubstanceInstanceSingleTableView.as_view(), name='substance-list'),
    path('substance/create/', views.SubstanceInstanceCreateView.as_view(), name='substance-create'),
    path('substance/update/<uuid:pk>', views.SubstanceInstanceUpdateView.as_view(), name='substance-update'),
    path('substance/delete/<uuid:pk>', views.SubstanceInstanceDeleteView.as_view(), name='substance-delete'),
    path('substance/<uuid:pk>/', views.SubstanceInstanceDetailView.as_view(), name='substance-detail'),

    path('polymer/list/', views.PolymerInstanceSingleTableView.as_view(), name='polymer-list'),
    path('polymer/create/', views.PolymerInstanceCreateView.as_view(), name='polymer-create'),
    path('polymer/update/<uuid:pk>', views.PolymerInstanceUpdateView.as_view(), name='polymer-update'),
    path('polymer/delete/<uuid:pk>', views.PolymerInstanceDeleteView.as_view(), name='polymer-delete'),
    path('polymer/<uuid:pk>/', views.PolymerInstanceDetailView.as_view(), name='polymer-detail'),

    path('mixture/list/', views.MixtureInstanceSingleTableView.as_view(), name='mixture-list'),
    path('mixture/create/', views.MixtureInstanceCreateView.as_view(), name='mixture-create'),
    path('mixture/update/<uuid:pk>', views.MixtureInstanceUpdateView.as_view(), name='mixture-update'),
    path('mixture/delete/<uuid:pk>', views.MixtureInstanceDeleteView.as_view(), name='mixture-delete'),
    path('mixture/<uuid:pk>/', views.MixtureInstanceDetailView.as_view(), name='mixture-detail'),
    
    #path('', views.IndexView.as_view(), name='lara_substances_store-main-view'),
    #path('<int:pk>/', views.SubstanceInstanceDetail.as_view(), name='substance-detail'),
    #path('safety/', views.SubstanceInstanceSafetyView.as_view(), name='substance-safety'),
    # path('', TemplateView.as_view(template_name='index.html'), name='lara_substances_store-main-view'),
    # path('subdir/', include('.urls')),
    path('', views.SubstanceInstanceSingleTableView.as_view(), name='substances-root'),
] # ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
