"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_substances_store admin *

:details: lara_django_substances_store admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev tables_generator lara_django_substances_store >> tables.py" to update this file
________________________________________________________________________
"""
# django Tests s. https://docs.djangoproject.com/en/4.1/topics/testing/overview/ for lara_django_substances_store
# generated with django-extensions tests_generator  lara_django_substances_store > tests.py (LARA-version)

import logging
import django_tables2 as tables


from .models import ExtraData, SubstanceInstance, SubstanceOrderItem, SubstancesWishlist, SubstanceBasket, SubstancesOrder, SubstancesLocalStore, PolymerInstance, PolymerOrderItem, PolymerWishlist, PolymerBasket, PolymersOrder, PolymerLocalStore, MixtureComponentInstance, MixtureInstance, MixtureOrderItem, MixtureWishlist, MixtureBasket, MixturesOrder, MixtureLocalStore

class ExtraDataTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_substances_store:extradata-detail', [tables.A('pk')]))

    class Meta:
        model = ExtraData

        fields = (
                'data_type',
                'namespace',
                'URI',
                'text',
                'XML',
                'JSON',
                'bin',
                'media_type',
                'IRI',
                'URL',
                'description',
                'file',
                'image')

class SubstanceInstanceTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name_full = tables.Column(linkify=('lara_django_substances_store:substance-detail', [tables.A('pk')]))

    class Meta:
        model = SubstanceInstance

        fields = (
                'namespace',
                'substance',
                'name',
                'name_full',
                'substance'
                'barcode1D',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'price',
                'currency',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'description',
                'amount_vol',
                'amount_mass',
                'purity',
                'labware',
                'hazard_classes',
                'hazard_statements',
                'precautionary_statements',
                'hazard_symbols',
                'storage_class',
                'literature',
                )

class SubstanceOrderItemTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_substances_store:substanceorderitem-detail', [tables.A('pk')]))

    class Meta:
        model = SubstanceOrderItem

        fields = (
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'substance_inst')

class SubstancesWishlistTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_substances_store:substanceswishlist-detail', [tables.A('pk')]))

    class Meta:
        model = SubstancesWishlist

        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

class SubstanceBasketTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_substances_store:substancebasket-detail', [tables.A('pk')]))

    class Meta:
        model = SubstanceBasket

        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

class SubstancesOrderTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_substances_store:substancesorder-detail', [tables.A('pk')]))

    class Meta:
        model = SubstancesOrder

        fields = (
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_date',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_pay_date',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state')

class SubstancesLocalStoreTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_substances_store:substanceslocalstore-detail', [tables.A('pk')]))

    class Meta:
        model = SubstancesLocalStore

        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location')

class PolymerInstanceTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_substances_store:polymer-detail', [tables.A('pk')]))

    class Meta:
        model = PolymerInstance

        fields = (
                'name',
                'namespace',
                
                'name_full',
                'barcode1D',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'price',
                'currency',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'description',
                'amount_vol',
                'amount_mass',
                'purity',
                'labware',
                'hazard_classes',
                'hazard_statements',
                'precautionary_statements',
                'hazard_symbols',
                'storage_class',
                'literature',
                'polymer')

class PolymerOrderItemTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_substances_store:polymerorderitem-detail', [tables.A('pk')]))

    class Meta:
        model = PolymerOrderItem

        fields = (
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'polymer_inst')

class PolymerWishlistTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_substances_store:polymerwishlist-detail', [tables.A('pk')]))

    class Meta:
        model = PolymerWishlist

        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

class PolymerBasketTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_substances_store:polymerbasket-detail', [tables.A('pk')]))

    class Meta:
        model = PolymerBasket

        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

class PolymersOrderTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_substances_store:polymersorder-detail', [tables.A('pk')]))

    class Meta:
        model = PolymersOrder

        fields = (
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_date',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_pay_date',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state')

class PolymerLocalStoreTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_substances_store:polymerlocalstore-detail', [tables.A('pk')]))

    class Meta:
        model = PolymerLocalStore

        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location')

class MixtureComponentInstanceTable(tables.Table):
    # adding link to column <column-to-be-linked>
    #name = tables.Column(linkify=('lara_django_substances_store:mixturecomponentinstance-detail', [tables.A('pk')]))

    class Meta:
        model = MixtureComponentInstance

        fields = (
                'namespace',
                'name',
                'name_full',
                'barcode1D',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'price',
                'currency',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'description',
                'amount_vol',
                'amount_mass',
                'purity',
                'labware',
                'hazard_classes',
                'hazard_statements',
                'precautionary_statements',
                'hazard_symbols',
                'storage_class',
                'literature',
                'mix_component')

class MixtureInstanceTable(tables.Table):
    # adding link to column <column-to-be-linked>
    name = tables.Column(linkify=('lara_django_substances_store:mixture-detail', [tables.A('pk')]))

    class Meta:
        model = MixtureInstance

        fields = (
                'name',
                'namespace',
                
                'name_full',
                'barcode1D',
                'registration_no',
                'vendor',
                'serial_no',
                'service_no',
                'price',
                'currency',
                'date_delivery',
                'end_of_warranty_date',
                'location',
                'description',
                'amount_vol',
                'amount_mass',
                'purity',
                'labware',
                'hazard_classes',
                'hazard_statements',
                'precautionary_statements',
                'hazard_symbols',
                'storage_class',
                'literature',
                'mixture')

class MixtureOrderItemTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_substances_store:mixtureorderitem-detail', [tables.A('pk')]))

    class Meta:
        model = MixtureOrderItem

        fields = (
                'quantity',
                'item_price_net',
                'item_price_gross',
                'item_shipping_price',
                'item_price_tax',
                'item_toll',
                'toll_number',
                'item_discount',
                'total_price_net',
                'mixture_inst')

class MixtureWishlistTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_substances_store:mixturewishlist-detail', [tables.A('pk')]))

    class Meta:
        model = MixtureWishlist

        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

class MixtureBasketTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_substances_store:mixturebasket-detail', [tables.A('pk')]))

    class Meta:
        model = MixtureBasket

        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified')

class MixturesOrderTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_substances_store:mixturesorder-detail', [tables.A('pk')]))

    class Meta:
        model = MixturesOrder

        fields = (
                'namespace',
                'name_full',
                'IRI',
                'user_ordered',
                'user_ordering',
                'order_date',
                'order_barcode',
                'order_company',
                'order_quote_date',
                'order_invoice_date',
                'shipping_company',
                'shipping_price',
                'budget_URL',
                'order_pay_date',
                'order_total_price_net',
                'order_total_price_gross',
                'order_total_price_tax',
                'order_total_price_toll',
                'order_total_price_currency',
                'order_state')

class MixtureLocalStoreTable(tables.Table):
    # adding link to column <column-to-be-linked>
    # <column-to-be-linked> = tables.Column(linkify=('lara_django_substances_store:mixturelocalstore-detail', [tables.A('pk')]))

    class Meta:
        model = MixtureLocalStore

        fields = (
                'namespace',
                'name_full',
                'user',
                'date_created',
                'date_modified',
                'location')

