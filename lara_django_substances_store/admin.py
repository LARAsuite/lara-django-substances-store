"""_____________________________________________________________________

:PROJECT: LARAsuite

*lara_django_substances_store admin *

:details: lara_django_substances_store admin module admin backend configuration.
         - 
:authors: mark doerr <mark.doerr@uni-greifswald.de>

.. note:: -
.. todo:: - run "lara-django-dev admin_generator lara_django_substances_store >> admin.py" to update this file
________________________________________________________________________
"""
# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import ExtraData, SubstanceInstance, SubstanceOrderItem, SubstancesWishlist, SubstanceBasket, SubstancesOrder, SubstancesLocalStore, PolymerInstance, PolymerOrderItem, PolymerWishlist, PolymerBasket, PolymersOrder, PolymerLocalStore, MixtureComponentInstance, MixtureInstance, MixtureOrderItem, MixtureWishlist, MixtureBasket, MixturesOrder, MixtureLocalStore


@admin.register(ExtraData)
class ExtraDataAdmin(admin.ModelAdmin):
    list_display = (
        'data_type',
        'namespace',
        'URI',
        'text',
        'XML',
        'JSON',
        'bin',
        'media_type',
        'IRI',
        'URL',
        'description',
        'extra_data_id',
        'file',
        'image',
    )
    list_filter = ('data_type', 'namespace', 'media_type')


@admin.register(SubstanceInstance)
class SubstanceInstanceAdmin(admin.ModelAdmin):
    list_display = (
        'name_full',
        'namespace',
        'substance',
        'name',
        
        'barcode1D',
       
      
        'registration_no',
        'vendor',
        'product_id',
        
       
       
        'price',
        'currency',
        'purchase_date',
        'date_delivery',
        'end_of_warranty_date',
        'location',
        'hash_SHA256',
        'description',
        'amount_vol',
        'amount_mass',
        'purity',
        'labware',
        'hazard_classes',
        'hazard_statements',
        'precautionary_statements',
        'hazard_symbols',
        'storage_class',
      
        
    )
    list_filter = (
        'namespace',
        'vendor',
        'manufacturing_date',
       
        'purchase_date',
        'date_delivery',
        'end_of_warranty_date',
        'location',
        'labware',
        'substance',
    )
    raw_id_fields = ('tags', 'users', 'extra_data')
    search_fields = ('name',)


@admin.register(SubstanceOrderItem)
class SubstanceOrderItemAdmin(admin.ModelAdmin):
    list_display = (
        'quantity',
        'item_price_net',
        'item_price_gross',
        'item_shipping_price',
        'item_price_tax',
   
        'item_discount',
        'total_price_net',
        'substance_orderitem_id',
        'substance_inst',
    )
    list_filter = ('substance_inst',)
    raw_id_fields = ('extra_data',)


@admin.register(SubstancesWishlist)
class SubstancesWishlistAdmin(admin.ModelAdmin):
    list_display = (
        'namespace',
        'name_full',
        'user',
        'date_created',
        'date_modified',
        'substance_wishlist_id',
    )
    list_filter = ('namespace', 'user', 'date_created', 'date_modified')
    raw_id_fields = ('substance_wish',)


@admin.register(SubstanceBasket)
class SubstanceBasketAdmin(admin.ModelAdmin):
    list_display = (
        'name_full',
        'namespace',
        
        'user',
        'date_created',
        'date_modified',
       
    )
    list_filter = ('namespace', 'user', 'date_created', 'date_modified')
    raw_id_fields = ('substances',)


@admin.register(SubstancesOrder)
class SubstancesOrderAdmin(admin.ModelAdmin):
    list_display = (
        'namespace',
        'name_full',
        'IRI',
        'user_ordered',
        'user_ordering',
        'order_date',
        'order_id_internal',
        'order_id_external',
        'order_barcode',
        'order_company',
        'order_quote_id',
        'order_quote_date',
        'order_invoice_id',
        'order_invoice_date',
        'shipping_company',
        'shipping_tracking_id',
        'shipping_price',
        'budget_URL',
        'order_pay_date',
        'order_total_price_net',
        'order_total_price_gross',
        'order_total_price_tax',
        'order_total_price_toll',
        'order_total_price_currency',
        'order_state',
        'substances_order_id',
    )
    list_filter = (
        'namespace',
        'user_ordered',
        'user_ordering',
        'order_date',
        'order_company',
        'order_quote_date',
        'order_invoice_date',
        'shipping_company',
        'order_pay_date',
        'order_total_price_currency',
        'order_state',
    )
    raw_id_fields = ('substance_order_items', 'extra_data')


@admin.register(SubstancesLocalStore)
class SubstancesLocalStoreAdmin(admin.ModelAdmin):
    list_display = (
        'namespace',
        'name_full',
        'user',
        'date_created',
        'date_modified',
        'substance_localstore_id',
        'location',
    )
    list_filter = (
        'namespace',
        'user',
        'date_created',
        'date_modified',
        'location',
    )
    raw_id_fields = ('substance_instances',)


@admin.register(PolymerInstance)
class PolymerInstanceAdmin(admin.ModelAdmin):
    list_display = (
         'name_full',
        'namespace',
        'name',
        'polymer',
       
        'barcode1D',
       
        'registration_no',
        'vendor',
        'product_id',
        'serial_no',
        'service_no',
        'manufacturing_date',
        'price',
        'currency',
        'purchase_date',
        'date_delivery',
        'end_of_warranty_date',
        'location',
       
        'description',
        'amount_vol',
        'amount_mass',
        'purity',
        'labware',
        'hazard_classes',
        'hazard_statements',
        'precautionary_statements',
        'hazard_symbols',
        'storage_class',
        
      
       
    )
    list_filter = (
        'namespace',
        'vendor',
        'manufacturing_date',
        'currency',
        'purchase_date',
        'date_delivery',
        'end_of_warranty_date',
        'location',
        'labware',
        'polymer',
    )
    raw_id_fields = ('tags', 'users', 'extra_data')
    search_fields = ('name',)


@admin.register(PolymerOrderItem)
class PolymerOrderItemAdmin(admin.ModelAdmin):
    list_display = (
        'quantity',
        'item_price_net',
        'item_price_gross',
        'item_shipping_price',
        'item_price_tax',
        'item_toll',
        'toll_number',
        'item_discount',
        'total_price_net',
        'polymer_orderitem_id',
        'polymer_inst',
    )
    list_filter = ('polymer_inst',)
    raw_id_fields = ('extra_data',)


@admin.register(PolymerWishlist)
class PolymerWishlistAdmin(admin.ModelAdmin):
    list_display = (
        'namespace',
        'name_full',
        'user',
        'date_created',
        'date_modified',
        'polymer_wishlist_id',
    )
    list_filter = ('namespace', 'user', 'date_created', 'date_modified')
    raw_id_fields = ('polymer_wish',)


@admin.register(PolymerBasket)
class PolymerBasketAdmin(admin.ModelAdmin):
    list_display = (
        'namespace',
        'name_full',
        'user',
        'date_created',
        'date_modified',
        'polymer_basket_id',
    )
    list_filter = ('namespace', 'user', 'date_created', 'date_modified')
    raw_id_fields = ('polymers',)


@admin.register(PolymersOrder)
class PolymersOrderAdmin(admin.ModelAdmin):
    list_display = (
         'name_full',
        'namespace',
       
        'user_ordered',
        'user_ordering',
        'order_date',
        'order_id_internal',
        'order_id_external',
        'order_barcode',
        'order_company',
        'order_quote_id',
        'order_quote_date',
        'order_invoice_id',
        'order_invoice_date',
        'shipping_company',
        'shipping_tracking_id',
        'shipping_price',
        'budget_URL',
        'order_pay_date',
        'order_total_price_net',
        'order_total_price_gross',
        'order_total_price_tax',
        'order_total_price_toll',
        'order_total_price_currency',
        'order_state',
        'polymers_order_id',
    )
    list_filter = (
        'namespace',
        'user_ordered',
        'user_ordering',
        'order_date',
        'order_company',
        'order_quote_date',
        'order_invoice_date',
        'shipping_company',
        'order_pay_date',
        'order_total_price_currency',
        'order_state',
    )
    raw_id_fields = ('polymer_order_items', 'extra_data')


@admin.register(PolymerLocalStore)
class PolymerLocalStoreAdmin(admin.ModelAdmin):
    list_display = (
         'name_full',
        'namespace',
       
        'user',
        'date_created',
        'date_modified',
        
        'location',
    )
    list_filter = (
        'namespace',
        'user',
        'date_created',
        'date_modified',
        'location',
    )
    raw_id_fields = ('polymer_instances',)


@admin.register(MixtureComponentInstance)
class MixtureComponentInstanceAdmin(admin.ModelAdmin):
    list_display = (
        'name_full',
        'namespace',
        'name',
        'concentration',
        'concentration_weight',
        
        'barcode1D',
        
        'registration_no',
        'vendor',
        'product_id',

      
        'date_delivery',
        'end_of_warranty_date',
        'location',
       
        'description',
        'amount_vol',
        'amount_mass',
        'purity',


        'storage_class',
       
      
       
    )
    list_filter = (
        'namespace',
        'vendor',
       
       
        'purchase_date',
        'date_delivery',
        'end_of_warranty_date',
        'location',
        'labware',
        
    )
    raw_id_fields = ('tags', 'users', 'extra_data')
    search_fields = ('name',)


@admin.register(MixtureInstance)
class MixtureInstanceAdmin(admin.ModelAdmin):
    list_display = (
        'name_full',
        'namespace',
        'name',
       
       
        'barcode1D',
        
        'registration_no',
        'vendor',
        'product_id',

        'manufacturing_date',
        'price',
        'currency',
        'purchase_date',
        'date_delivery',
        'end_of_warranty_date',
        'location',
      
        'description',
        'amount_vol',
        'amount_mass',
        'purity',
        'labware',
        
        'hazard_symbols',
        'storage_class',
    )
    list_filter = (
        'namespace',
        'vendor',
        'manufacturing_date',
        'currency',
        'purchase_date',
        'date_delivery',
        'end_of_warranty_date',
        'location',
        'labware',
       
    )
    raw_id_fields = ('tags', 'users', 'extra_data', 'components')
    search_fields = ('name',)


@admin.register(MixtureOrderItem)
class MixtureOrderItemAdmin(admin.ModelAdmin):
    list_display = (
        'quantity',
        'item_price_net',
        'item_price_gross',
        'item_shipping_price',
        'item_price_tax',
        'item_toll',
        'toll_number',
        'item_discount',
        'total_price_net',
        'mixture_orderitem_id',
        'mixture_inst',
    )
    list_filter = ('mixture_inst',)
    raw_id_fields = ('extra_data',)


@admin.register(MixtureWishlist)
class MixtureWishlistAdmin(admin.ModelAdmin):
    list_display = (
        'namespace',
        'name_full',
        'user',
        'date_created',
        'date_modified',
        'mixture_wishlist_id',
    )
    list_filter = ('namespace', 'user', 'date_created', 'date_modified')
    raw_id_fields = ('mixture_wish',)


@admin.register(MixtureBasket)
class MixtureBasketAdmin(admin.ModelAdmin):
    list_display = (
        'namespace',
        'name_full',
        'user',
        'date_created',
        'date_modified',
        'mixture_basket_id',
    )
    list_filter = ('namespace', 'user', 'date_created', 'date_modified')
    raw_id_fields = ('mixtures',)


@admin.register(MixturesOrder)
class MixturesOrderAdmin(admin.ModelAdmin):
    list_display = (
         'name_full',
        'namespace',
       
        'IRI',
        'user_ordered',
        'user_ordering',
        'order_date',
        'order_id_internal',
        'order_id_external',
        'order_barcode',
        'order_company',
        'order_quote_id',
        'order_quote_date',
        'order_invoice_id',
        'order_invoice_date',
        'shipping_company',
        'shipping_tracking_id',
        'shipping_price',
        'budget_URL',
        'order_pay_date',
        'order_total_price_net',
        'order_total_price_gross',
        'order_total_price_tax',
        'order_total_price_toll',
        'order_total_price_currency',
        'order_state',
      
    )
    list_filter = (
        'namespace',
        'user_ordered',
        'user_ordering',
        'order_date',
        'order_company',
        'order_quote_date',
        'order_invoice_date',
        'shipping_company',
        'order_pay_date',
        'order_total_price_currency',
        'order_state',
    )
    raw_id_fields = ('mixture_order_items', 'extra_data')


@admin.register(MixtureLocalStore)
class MixtureLocalStoreAdmin(admin.ModelAdmin):
    list_display = (
        'namespace',
        'name_full',
        'user',
        'date_created',
        'date_modified',
       
        'location',
    )
    list_filter = (
        'namespace',
        'user',
        'date_created',
        'date_modified',
        'location',
    )
    raw_id_fields = ('mixture_instances',)
