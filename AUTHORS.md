
# Acknowledgements and Credits

The LARA-django Substances Store project thanks


Contributors
------------

* The whole python and python-django team
* Mickey Kim <mickey.kim@genomicsengland.co.uk>  - Thanks for the phantastic cookiecutter pypackage template !
* Daniel Roy Greenfeld  <a href="https://github.com/pydanny">pydanny</a> and his cookiecutter-django team


Development Lead
----------------

* mark doerr <mark.doerr@uni-greifswald.de>